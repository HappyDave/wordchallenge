// Subject: Programa principal d'obte_paraules

#include <iostream>
using namespace std;

#include <esin/error>
#include <esin/util>
#include "obte_paraules.hpp"
#include "anagrames.hpp"
#include "diccionari.hpp"

int main () {
    diccionari D;
    
    D.insereix("OSAR");
    D.insereix("OTRA");
    D.insereix("RASO");
    D.insereix("RATO");
    D.insereix("ROSA");
    D.insereix("ROTA");
    D.insereix("SACO");
    D.insereix("SOTA");
    D.insereix("TACO");
    D.insereix("TARO");
    D.insereix("TASO");

//     < [ACTO, ASCO, CARO, CASO, CATO, COSA, COTA, OCAS, ORAS, OTRA, SACO, OSAR, RATO, SOTA, TACO, RASO, ROTA, TASO, TOCA, ROSA, TARO, TOSA, TRAS]
//     ---
//     > [ACTO, ASCO, CARO, CASO, CATO, COSA, COTA, OCAS, ORAS, OSAR, OTRA, RASO, RATO, ROSA, ROTA, SACO, SOTA, TACO, TARO, TASO, TOCA, TOSA, TRAS]
    
    anagrames A(D);
    
	string s = "TSRAOC";
	list<string> llista;
//  	A.mateix_anagrama_canonic(s, llista);
	
// 	list<string>::iterator it = llista.begin();
// 		cout << "-> Resultat llista: " << endl;
// 	while(it != llista.end()) {
// 	  cout << *it << ",";
// 	  ++it;
// 	}
// 	cout << endl;
    
    list<string> paraules;

	
// 	paraules.push_back("ARCO");
// 	paraules.push_back("ARO");
// 	paraules.push_back("CARO");
// 	paraules.push_back("CARRO");
// 	paraules.push_back("CORO");
// 	paraules.push_back("CORRO");
// 	paraules.push_back("ARO");
// 	paraules.push_back("CROAR");
// 	paraules.push_back("OCA");
// 	paraules.push_back("ORAR");
// 	paraules.push_back("ORCA");
// 	paraules.push_back("ORCO");
// 	paraules.push_back("ACORRO");
// 	paraules.push_back("RARO");
// 	paraules.push_back("CORROA");
// 	paraules.push_back("ROCA");
// 	paraules.push_back("ACORO");
// 	
// 	list<string>::iterator itf = paraules.begin();
// 	list<string>::iterator its = paraules.begin();
// 	its++;
	
// 	if((int)(*itf) < (int)(*its)) cout << "petit" << endl;
	
// 	paraules.sort();
    
    obte_paraules::obte_paraules(4, "TSRAOC", A, paraules);
    
    cout << "OSAR, OTRA, RASO, RATO, ROSA, ROTA, SACO, SOTA, TACO, TARO, TASO," << endl;
    
    while(!paraules.empty()) {
	cout << paraules.front()<< ", ";
	paraules.pop_front();
    } cout << endl;
  
}