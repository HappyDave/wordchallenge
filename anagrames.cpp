#include "anagrames.hpp"
#include "word_toolkit.hpp"

// Construeix el conjunt d’anagrames de totes les paraules de longitud >= 3 del diccionari D.
// Cost: Ɵ(n)
anagrames::anagrames(const diccionari& D) throw(error) {
    
    list<string> llista;
    D.llista_paraules(3, llista);
    
    if (llista.size() == 0) {
	  _M = 0;
	  _base = 1;
	  _taula = new node_hash*[_M];
	  
    }else {    
	    _base = 1;
	    
	    // Calcular la potència de 2 més propera al num d'elements que volem guardar
	    while(llista.size() > _base){
		_base = _base + _base;
	    }
	    
	    // Petit ajust per a hashtables inferiors a 64
	    if(llista.size() < 64) _M = _base;
	    else _M = llista.size();
	    
	    // Inicialització d'arrays de punters (node_hash) a NULL amb mida = _M
	    _taula = new node_hash*[_M];
	    for(nat i = 0; i < _M; ++i) _taula[i] = NULL;
	    
	    // Inserir elements a la hashtable
	    list<string>::const_iterator it = llista.begin();
	    while(it != llista.end()) {
		insereix(gen_clau(*it), (*it));
		++it;
	    }
    }
}

// Tres grans. Constructor per còpia, operador d’assignació i destructor.
// Cost: Ɵ(n)
anagrames::anagrames(const anagrames& A) throw(error) {
	_taula = new node_hash*[A._M];
	for(nat i = 0; i < A._M; ++i) _taula[i] = copiar_hashtable(A._taula[i]);
	_M = A._M;
	_base = A._base;
}

// Cost: Ɵ(n)
anagrames& anagrames::operator=(const anagrames& A) throw(error) {
	if(this != &A){
		anagrames aux(A);
		swap(aux);
	}
	return *this;
}

// Cost: Ɵ(n)
anagrames::~anagrames() throw() {
	for(nat i = 0; i < _M; ++i) eliminar_hashtable(_taula[i]);
	delete[] _taula;
	_taula = NULL;
}

// Retorna la llista de paraules p del diccionari usat al crear l’estructura d’anagrames tals que
// anagrama_canonic(p) = a. Llança un error si les lletres de a no estan en ordre ascendent.
// Cost: Ɵ(a.size())
void anagrames::mateix_anagrama_canonic(const string& a, list<string>& L) const throw(error) {
   if(!word_toolkit::es_canonic(a)) throw error(NoEsCanonic);
   else if(_M > 0) {
	  nat k = gen_clau(a);
	  nat i = funcio_dispersio(k);
	  node_hash *p = _taula[i];
	  bool trobat = false;
	  
	  while(p != NULL && !trobat) {
	  
		if(p->_k == k) trobat = true;
		else p = p->_seg;
	  }
	  
	  if(trobat) L = p->_valors;
   }
}

// Funció de dispersió de la HashTable
// Cost Ɵ(1)
nat anagrames::funcio_dispersio(nat n) const throw(){

	n = n >> 5;		// n / 16; 2^5 = 16; 5 és el número de bits que volem eliminar de la part menys significativa del num.

	n = n % _base;		// n % _base; _base = 2^X -> X són els bits menys significatius que ens volem quedar.
	
	return n % _M;
}

// Genera una clau comuna per a totes les paraules que tinguin el mateix anagrama canònic
// Cost Ɵ(k), on k = nº lletres de la paraula
nat anagrames::gen_clau(const string &paraula) const throw() {

	nat clau = 0;
	for(nat j = 0; j < paraula.size(); ++j) {
	    clau += paraula[j] * (paraula[j] - 64);
	}
	return clau * paraula.size();
}

// Constructora per a l'estructura node_hash; node_hash es contruit amb una list, la qual tindrà només l'string "v" que es passa per referència
// Cost Ɵ(1)
anagrames::node_hash::node_hash(const nat &k, const string &v, node_hash* seg) throw(error) {
  
	_k = k;
	_valors.push_back(v);
	_seg = seg;
}

// Constructora per a l'estructura node_hash; node_hash es construit amb la list "valors" que es passa per referència
// Cost Ɵ(1)
anagrames::node_hash::node_hash(const nat &k, const list<string> &valors, node_hash* seg) throw(error) {
  
	_k = k;
	_valors = valors;
	_seg = seg;
}

// Insereix una clau i el seu valor dins de la Hashtable
// Cost Ɵ(1)
void anagrames::insereix(const nat &k, const string &v) throw(error) {
	 
      	nat i = funcio_dispersio(k);
	node_hash *p = _taula[i];
	bool hi_es = false;
	
	while(p != NULL && !hi_es) {
	
	      if(p->_k == k) hi_es = true;
	      else p = p->_seg;
	}
	if(p == NULL) {	
	      _taula[i] = new node_hash(k, v, _taula[i]);
	}
	else {
	      p->_valors.push_back(v);
	}
}

// Esborra tots els nodes_hash a partir del node_hash "p"
// Cost Ɵ(n)
void anagrames::eliminar_hashtable(node_hash *p) {
    if(p != NULL){
      
	eliminar_hashtable(p->_seg);
	delete p;
    
    }
}

// Realitza una còpia nova de l'estructura a partir del node_hash "p"
// Cost Ɵ(n)
anagrames::node_hash* anagrames::copiar_hashtable(const node_hash* p) throw(error) {

	node_hash *c = NULL;
	
	if(p != NULL){
	  
		try{
		  
		    c = new node_hash(p->_k, p->_valors);
		    c->_seg = copiar_hashtable(p->_seg);
		
		}catch(const error &e){
		
			delete c;
			throw;
		}
		
	}
	
	return c;
}

// Intercanvia els atributs del anagrama actual amb el que es passa per referència
// Cost Ɵ(1)
void anagrames::swap(anagrames &ana) throw() {
	node_hash **aux_taula = _taula;
	_taula = ana._taula;
	ana._taula = aux_taula;
  	nat aux_M = _M;
	_M = ana._M;
	ana._M = aux_M;
	nat aux_base = _base;
	_base = ana._base;
	ana._base = aux_base;
}