#include "iter_subset.hpp"

// Construeix un iterador sobre els subconjunts de k elements de {1,..., n} si k > n no hi ha res a recòrrer.
// Cost: Ɵ(n), on n = k
iter_subset::iter_subset(nat n, nat k) throw(error) {
    if(k > n) _end = true;
    else {
	_n = n;
	_k = k;
	_end = false;
	for(int i = 1; i <= _k; i++) _subconjunt.push_back(i);
    }
}

// Tres grans. Constructor per còpia, operador d’assignació i destructor.
// Cost: Ɵ(n)
iter_subset::iter_subset(const iter_subset& its) throw(error) {
    _subconjunt = its._subconjunt;
    _n = its._n;
    _k = its._k;
    _end = its._end;
}
// Cost: Ɵ(n)
iter_subset& iter_subset::operator=(const iter_subset& its) throw(error) {
    _subconjunt = its._subconjunt;
    _n = its._n;
    _k = its._k;
    _end = its._end;
}
// Cost: Ɵ(n)
iter_subset::~iter_subset() throw() {}

// Retorna cert si l’iterador ja ha visitat tots els subconjunts de k elements presos d’entre n; o dit
// d’una altra forma, retorna cert quan l’iterador “apunta” a un subconjunt sentinella fictici que
// queda a continuació de l’últim subconjunt vàlid.
// Cost: Ɵ(1)
bool iter_subset::end() throw() {
    return _end;
}

// Operador de desreferència. Retorna el subconjunt “apuntat” per l’iterador; llança un error si
// l’iterador apunta al sentinella.
// Cost: Ɵ(1)
subset iter_subset::operator*() const throw(error) {
    if(_end) throw error(IterSubsetIncorr);
    return _subconjunt;
}

// Operador de preincrement. Avança l’iterador al següent subconjunt en la seqüència i el retorna;
// no es produeix l’avançament si l’iterador ja apuntava al sentinella.
// Cost: Ɵ(log n)
iter_subset& iter_subset::operator++() throw() {
    if(!_end) {
	combinatoria(_n, _k);
    }
    return *this;
}

// Operador de postincrement. Avança l’iterador al següent subconjunt en la seqüència i retorna el
// seu valor previ; no es produeix l’avançament si l’iterador ja apuntava al sentinella.
// Cost: Ɵ(n)
iter_subset iter_subset::operator++(int) throw() {
    iter_subset tmp(*this);
    ++(*this);
    return tmp;
}

// Operadors relacionals.
// Cost: Ɵ(n)
bool iter_subset::operator==(const iter_subset& c) const throw() {
    bool iguals = (_subconjunt == c._subconjunt) && (_n == c._n) && (_k == c._k) && (_end == c._end); 
    if (_k == 0 && c._k == 0 && _end == c._end) iguals = true;		//Cas especial per quan k == 0
    return iguals;
}
// Cost: Ɵ(n)
bool iter_subset::operator!=(const iter_subset& c) const throw() {
    return !(*this == c);
}

// Crea la següent combinatoria possible (No importa l'ordre i sense repetir elements)
// Cost: Ɵ(log n)
void iter_subset::combinatoria(nat n, nat k) throw() {
    if(k == 0 || (n - k + 1) == _subconjunt[0]) _end = true;
    else {
	int pos = k - 1;
	while(_subconjunt[pos] == n) {
	    pos--;
	    n--;
	}
	_subconjunt[pos] += 1;
	for(int i = pos + 1; i < k; i++) _subconjunt[i] = _subconjunt[i - 1] + 1;
    }
}