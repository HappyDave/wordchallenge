#include "word_toolkit.hpp"

//Retorna cert si i només si les lletres de l’string s estan en ordre lexicogràfic ascendent.
// Cost Ɵ(n)
bool word_toolkit::es_canonic(const string& s) throw(){
  
  char c = 0;
  string::const_iterator itr = s.begin();
  string::const_iterator itr_end = s.end();
  
  bool canonic = true;
  
  while(itr != itr_end && canonic){	 
	 
	 canonic = (c <= *itr);
	 c = *itr;
	 itr++;
  }
  
  return canonic;
  
}
//Retorna l’anagrama canònic de l’string s. Veure la classe anagrames per saber la definició d’anagrama canònic.
// Cost Ɵ(n·log(n))
string word_toolkit::anagrama_canonic(const string& s) throw(){
 
int i = 0;
int q = 0;
string aux;
char c_aux;

	
	while(i < s.size()){
			
			if(aux[q] <= s[i]){
				aux.resize (i+1);
				q=i;
				aux.at(q)=s.at(i);
				i++;
				
			}else{

				 aux.resize (i+1);
				 c_aux = aux.at(q);
				 aux.at(q+1)=c_aux;
				 aux.at(q) = s.at(q+1);
				
				
				 while (q > 0 && aux.at(q-1) > aux.at(q)){
					c_aux = aux.at(q);
					aux.at(q) = aux.at(q-1);
					aux.at(q-1) = c_aux;
					q--;
				 }
				 q=i;;
				 i++;
			}
	}

     return aux;   
  
}

/*Retorna el caràcter que no apareix a l’string excl i és el més freqüent en la llista de paraules L,
sent L una llista no buida de paraules formades exclusivament amb lletres majúscules de la ’A’
a la ’Z’—excloses la ’Ñ’, ’Ç’, majúscules accentuades, . . . En cas d’empat, es retornaria el caràcter
alfabèticament menor. Si l’string excl inclou totes les lletres de la ’A’ a la ’Z’ es retorna el caràcter
’\0’, és a dir, el caràcter de codi ASCII 0.*/
//Cost Ɵ(n²) on es fara un recorregut de cada paraula de la llista, comprovant dins de la llista d'exclosos.
char word_toolkit::mes_frequent(const string& excl, const list<string>& L) throw(){
  
      pair <int,int> maxim (0,0);				 //crearem un pair per emmagatzemar la posició hon es trobara y el número de repeticions
  
      if(!L.empty()){
	    int contador[26] = {};				//creacio d'un array de naturals per poder controlar les paraules trobades
	    list<string>::const_iterator itr = L.begin();
	    list<string>::const_iterator itr_end = L.end();
	    int pos = 0;
	    
	    for(int j = 0; j < excl.size(); j++) contador[excl[j] - 65] = -1;
	    
	    while(itr != itr_end){
		    
		    for(int i = 0; (*itr)[i] != '\0'; i++) {
			   pos = (*itr)[i] - 65;
			   
			   if(contador[pos] >= 0) {
				contador[pos] += 1;	
			   
				if(contador[pos] > maxim.second){
				    maxim.first = pos + 65;
				    maxim.second = contador[pos];
				
				}else if (contador[pos] == maxim.second){
				    maxim.first = min(pos + 65, maxim.first);
				}
			   }
		    }
		    itr++;
	    }
      }
      return maxim.first;
}
