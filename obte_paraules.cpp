#include "obte_paraules.hpp"
#include "word_toolkit.hpp"
#include "iter_subset.hpp"


// Retorna la llista de paraules que es poden formar usant k lletres de la paraula s. Llança error si
// k és major que la longitud de l’string s o k < 3.
// Cost: Ɵ(((n sub k) · n), on n = s.size()
void obte_paraules::obte_paraules(nat k, const string& s, const anagrames& A, list<string>& paraules) throw(error) {
	if(k > s.size() || k < 3) throw error(LongitudInvalida);
	else {
		string canonic = word_toolkit::anagrama_canonic(s);
		iter_subset iter(canonic.size(), k);
		
		string aux;
		list<string> aux_list;
		while(!iter.end()){
		    
		    for(nat i = 0; i < (*iter).size(); ++i) aux.push_back(canonic[((*iter)[i])-1]);
		    
		    A.mateix_anagrama_canonic(aux,aux_list);
		    
		    list<string>::iterator it = paraules.begin();
		    list<string>::iterator it_aux = aux_list.begin();
			
		    while(it != paraules.end() && it_aux != aux_list.end()) {
			    if((*it).size() > (*it_aux).size()) {
				    paraules.insert(it, *it_aux);
				    ++it_aux;
				    ++it;
				    aux_list.pop_front();
			    }
			    else if((*it).size() < (*it_aux).size()) {
				    ++it;
			    }
			    else {
				    if((*it).compare(*it_aux) < 0) ++it;
				    else if ((*it).compare(*it_aux) > 0){
					    paraules.insert(it, *it_aux);
					    ++it_aux;
					    aux_list.pop_front();
				    }
				    else {
					   ++it_aux;
					   ++it;
				    }
			    }
		    }
		    while(it_aux != aux_list.end()) {
			    paraules.push_back(*it_aux);
			    ++it_aux;
			    aux_list.pop_front();
		    }
		    
		    aux.clear();  // Cost n, on n = aux.size()
		    iter++;
		}
	}
}
// Retorna la llista de paraules que es poden formar usant 3 o més lletres de la paraula s. La llista
// està ordenada de menys a més longitud; a igual longitud les paraules estan en ordre alfabètic
// creixent. Llança un error si l’string s té menys de tres lletres.
// Cost: Ɵ(x · ((n sub k) · n)), on n = s.size() i x = s.size() - 2
void obte_paraules::obte_paraules(const string& s, const anagrames& A, list<string>& paraules) throw(error) {
	if(s.size() < 3) throw error(LongitudInvalida);
	for(nat k = 3; k <= s.size(); ++k){
		obte_paraules(k,s,A,paraules);
	}
}