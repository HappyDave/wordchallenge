// Subject: Programa principal del iter_subset

#include <iostream>
using namespace std;

#include "iter_subset.hpp"

int main () {
    iter_subset A(6,4);
    while(!A.end()) {
	cout << (*A)[0] << ", ";
	cout << (*A)[1] << ", ";
	cout << (*A)[2] << ", ";
	cout << (*A)[3] << endl;
	A++;
    }
    
    iter_subset B(A);
    iter_subset C(1,1);
    A = C;
    
}