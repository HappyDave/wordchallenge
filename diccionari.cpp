#include "diccionari.hpp"

// Construeix un diccionari que conté únicament una paraula: la paraula buida.
// Cost: Ɵ(1)
diccionari::diccionari() throw(error) {
   
    _arrel = new node_dicc;
    _arrel-> _clau = '#';
    _arrel-> _esq = _arrel->_dret = _arrel->_cen = NULL;
    _cnt = 0;
}       

// Constructor per còpia
// Cost Ɵ(n)
diccionari::diccionari(const diccionari& D) throw(error) {
	_cnt = D._cnt;
	_arrel = copia_diccionari(D._arrel);
}

// Operador assignació 
// Cost Ɵ(n)
diccionari& diccionari::operator=(const diccionari& D) throw(error) {
  
  if(this!= &D){
		node_dicc* n = copia_diccionari(D._arrel);
		esborra_diccionari(_arrel);
		_arrel = n;
		_cnt = D._cnt;
	}
	return *this;
}

// Destructora
// Cost Ɵ(n)
diccionari::~diccionari() throw() {
	esborra_diccionari(_arrel);
}     

// Afegeix la paraula p al diccionari; si la paraula p ja formava part del diccionari, l’operació no té cap efecte.
// Cost Ɵ(n) on n = clau.longitud()
void diccionari::insereix(const string& p) throw(error) {

	_arrel -> _cen = insereix(p + "#", _arrel -> _cen, 0);
}

/* Retorna el prefix més llarg de p que és una paraula que pertany al diccionari, o dit d’una forma
més precisa, retorna la paraula més llarga del diccionari que és prefix de p*/
// Cost Ɵ(n) on n = clau.longitud()
string diccionari::prefix(const string& p) const throw(error) {
	string prefix;
	bool trobat = true;
	prefix_r(p, prefix, _arrel -> _cen, 0, trobat);
	return prefix;
}

/* Retorna la llista de paraules del diccionari que satisfan el patró especificat en el vector d’strings
q, en ordre alfabètic ascendent*/
// Cost Ɵ(3^n)
void diccionari::satisfan_patro(const vector<string>& q, list<string>& L) const throw(error) {
  
	  string trobat;
	  nat k = q.size();
	  satisfan_patro_r(k, trobat, L, _arrel -> _cen, 0, q.begin());
}

/* Retorna una llista amb totes les paraules del diccionari de longitud major o igual a k en ordre
alfabètic ascendent*/
// Cost Ɵ(n)
void diccionari::llista_paraules(nat k, list<string>& L) const throw(error) {
	
	string trobat;
	llista_paraules_r(k, trobat, L, _arrel -> _cen);
}

// Retorna el nombre de paraules en el diccionari
// Cost Ɵ(1)
nat diccionari::num_pal() const throw() {
  
  return _cnt;
  
}

// Esborra tots els nodes a partir del node "d"
// Cost Ɵ(n)
void diccionari::esborra_diccionari(node_dicc* d) throw(error){
  
	if(d != NULL){
		esborra_diccionari(d -> _esq);
		esborra_diccionari(d -> _dret);
		esborra_diccionari(d -> _cen);
		delete (d);
	}
}

// Realitza una còpia nova de l'estructura a partir del node "origen"
// Cost Ɵ(n)
diccionari::node_dicc* diccionari::copia_diccionari(const node_dicc* origen) throw(error){
	node_dicc* desti = NULL;
	if(origen != NULL){
		desti = new node_dicc;
		try{
			desti->_clau = origen->_clau;
			desti->_esq = copia_diccionari(origen->_esq);
			desti->_dret = copia_diccionari(origen->_dret);
			desti->_cen = copia_diccionari(origen->_cen);
		}
		catch(error){
			delete(desti);
			throw;
		}
	}
	return desti;
}

// Subdivideix la clau en varies subclaus, i crea un node per cada subclau.
// Cost Ɵ(n) on n = clau.longitud()
diccionari::node_dicc* diccionari::insereix(const string& clau, node_dicc* a, nat i) throw(error) {
	if(a == NULL){
		if(i != clau.size()) {		//Cas Repetició d'una paraula dins al diccionari
		    a = new node_dicc;

		    a->_clau = clau[i];
		    a->_esq = a->_dret = a->_cen = NULL;
		    
		    if(clau[i] == '#') _cnt++;	//Només incrementarem el contador de #paraules quan arribem al final (#)
		    
			    
		    try {
			    if (i < clau.size()-1) {
				    a->_cen = insereix(clau, a->_cen,++i);
			    }
			}
		    catch(error){
			    delete(a);
			    throw;
		    }
		}
	}
	else{
		if(clau[i] < a->_clau) a->_esq = insereix(clau, a->_esq,i);
		else if(clau[i] > a->_clau) a->_dret = insereix(clau, a->_dret,i);
		else a->_cen = insereix(clau, a->_cen,++i);
	}
	return a;
  }

// Guarda la paraula més gran possible semblant a la clau sol·licitada, utilitza "prefix" per emmagatzemar cada clau de cada node i així poder recuperar les paraules guardades en el diccionari.
// Cost Ɵ(n) on n = clau.longitud()
/* En el pitjor dels casos n s'incrementara degut a les crides recursives als nodes de l'esquerra a la tornada de la recursivitat*/
void diccionari::prefix_r(const string &clau, string &prefix, node_dicc* n, nat i, bool &trobat) const throw(error) {
	if (n != NULL) {
	    
		if (i == clau.length()) trobat = (n->_clau == '#');
		else {
			if (n->_clau > clau[i]) {
			    prefix_r(clau, prefix, n->_esq, i, trobat); 
			}else if (n->_clau < clau[i]) {
			    prefix_r(clau, prefix, n->_dret, i, trobat);
			}else {
			    prefix.push_back(clau[i]);
			    prefix_r(clau, prefix, n->_cen, i+1, trobat);
			    if(!trobat) prefix.erase(prefix.end() - 1);
			}
			
			if (!trobat) {
			    if (n -> _clau == '#') trobat = true;
			    else prefix_r(clau, prefix, n->_esq, i, trobat);
			}
		}
	}else{
		trobat = false;
	}
}

// Guarda totes les paraules del TST en una llista, utilitza "trobat" per emmagatzemar cada clau de cada node i així poder recuperar les paraules guardades en el diccionari.
// Cost Ɵ(n)
void diccionari::llista_paraules_r(nat &k, string &trobat, list<string>& L, node_dicc* n) const throw(error) {
	  if (n != NULL) {
	      if(n->_clau == '#'){ 
		  if(trobat.size() >= k) {
		      L.push_back(trobat);
		  }
		  llista_paraules_r(k, trobat, L, n->_dret);
	      }
	      else {
		  llista_paraules_r(k, trobat, L, n->_esq);
		  trobat.push_back(n->_clau);
		  llista_paraules_r(k, trobat, L, n->_cen);
		  trobat.erase(trobat.end() - 1);
		  llista_paraules_r(k, trobat, L, n->_dret);
	      }
	      
	  }
  }
  
// Cost Ɵ(3^n), on n = la combinació possible de patrons que es puguin fer
void diccionari::satisfan_patro_r(nat &k, string &paraula, list<string>& L, node_dicc* n, nat i, vector<string>::const_iterator it) const throw(error) {
	
	if (n != NULL && paraula.size() <= k) {
		if(paraula.size() == k) {
			if(n->_clau == '#')  L.push_back(paraula); 
			else  satisfan_patro_r(k, paraula, L, n->_esq, i, it);

		}else {
			if(n->_clau > (*it)[i]) {
			    satisfan_patro_r(k, paraula, L, n->_esq, i, it);
			    ++i;
			}
			
			bool trobat, final_patro;
			trobat = final_patro = false;
			
			while(i < (*it).size() && !trobat && !final_patro) {
			    if((*it)[i] > n->_clau) final_patro = true;
			    else {
				trobat = (n->_clau == (*it)[i]);
				i++;
			    }
			}
			
			if(trobat) {
			    paraula.push_back(n->_clau);
			    satisfan_patro_r(k, paraula, L, n->_cen, 0, ++it);
			    paraula.erase(paraula.end() - 1);
			    --it;
			}
			
			if (i < (*it).size()) satisfan_patro_r(k, paraula, L, n->_dret, i, it);
	      }
	}
}

  
