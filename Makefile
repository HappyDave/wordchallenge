JP=driver_joc_par.cpp obte_paraules.cpp word_toolkit.cpp diccionari.cpp anagrames.cpp iter_subset.cpp
WORD_TOOLKIT=word_toolkit.cpp main_word_toolkit.cpp
ANAGRAMES=anagrames.cpp main_anagrames.cpp diccionari.cpp word_toolkit.cpp
ITER_SUBSET=iter_subset.cpp main_iter_subset.cpp
OBTE_PARAULES=obte_paraules.cpp main_obte_paraules.cpp word_toolkit.cpp diccionari.cpp anagrames.cpp iter_subset.cpp
DICCIONARI=main_diccionari.cpp diccionari.cpp
all:
main_obte_paraules: $(OBTE_PARAULES)
	g++ -o $@.e $+ -lesin
main_word_toolkit: $(WORD_TOOLKIT)
	g++ -o $@.e $+ -lesin
main_anagrames: $(ANAGRAMES)
	g++ -o $@.e -g $+ -lesin
main_iter_subset: $(ITER_SUBSET)
	g++ -o $@.e $+ -lesin
main_diccionari:$(DICCIONARI)
	g++ -o $@.e -g $+ -lesin
jp: $(JP)
	g++ -o $@.e -g $+ -lesin
clean:
	rm -rf *.o *.e *.*~ *~
